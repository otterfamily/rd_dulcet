from flask import Flask, jsonify, request, send_from_directory, redirect, render_template
from flask_cors import CORS
from midi_player import midi_player
import json
import sys
import glob, os
import serial
import serial.tools.list_ports
import os.path
SESSION_URL = "./sessions/session.json"
SESSION_FALLBACK_URL = "./sessions/fallback.json"

ser = serial.Serial()
ser.baudrate = 9600
port = sys.argv[1]

current_state = ""
app = Flask(__name__, static_url_path='', static_folder="")
app.config.from_object(__name__)
CORS(app)

mplayer = midi_player()
mplayer.set_serial(ser)
# mplayer.load("./music/mary_had_a_little_lamb_pno.mid")

session = {} #at the moment, only support one user at a time - can add more things like multi user later

def load_last_session(s,sfb):

    try:
        f = open(s)
        f = json.load(f)
        return f      
    except:
        try:
            with open(sfb,"r") as contents:
                f = json.load(contents)
                return f
        except:
            print("session file was missing on startup. make sure fallback is in place")
            sys.exit()

def save_session(s,url):
    with open(url,'w') as outfile:
        f = json.dump(s,outfile)
def check_available_songs():
    files = []
    for file in os.listdir("./music"):
        if file.endswith(".mid") or file.endswith(".midi"):
            files.append({
                    "name":file,
                    "selected":False
                })
    print(files)
    session['available_songs'] = files

session = load_last_session(SESSION_URL,SESSION_FALLBACK_URL) #when restarting, load where we last left off
check_available_songs()
save_session(session,SESSION_URL) #generate a session.json if this is the first time running on machine


@app.route('/command/<path:path>')
def command(path):
    print(path)
    if path == 'play':
        session['playing'] = True
        mplayer.play()
    elif path == 'pause':
        session['player']['playing'] = False
        mplayer.pause()
    elif path == 'stop':
        session['player']['playing'] = False
        session['player']['progress']=0
        mplayer.stop()
    elif path == 'mute':
        mplayer.toggleMute()
    elif path == 'back':
        mplayer.stepBack()
    elif path == 'load':         
        try:
            mplayer.stop()
            url = request.args.get('url')
            mplayer.load("./music/"+url)
            for song in session['available_songs']:
                if url in song["name"]:
                    song["selected"] = True
                else:
                    song["selected"]  = False
            print("loaded successfully")
            return "loaded successfully"
        except:
            print("could not load the requested file")
            return "could not load requested file"
    elif path == 'connect':
        try:
            mplayer.stop()
            if ser.is_open:
                ser.close()
            device = request.args.get('port')
            ser.port = device
            ser.open()
            print("okay so far")
            if ser.is_open:
                session['connected'] = True
            else:
                session['connected'] = False
        except:
            print("could not connect to port")
            return "could not connect to port"
    return redirect('/controls')
   
@app.route("/get_session")
def get_session():
    session['playing'] = mplayer.playing
    serial_status = []
    available = serial.tools.list_ports.comports()
    for port in available:
        p = {"port":port.location,"device_name":port.device, "status":"N/A"}
        if p['device_name'] == ser.port:
            p['status'] = "Connected"
        else:
            p['status'] = "Not Connected"
        serial_status.append(p)
    session['serial_status'] = serial_status
    save_session(session, SESSION_URL)
    return jsonify(session)



@app.route('/controls') 
def static_file():
    return app.send_static_file("./controls.html")

@app.route('/') 
def home():
    return redirect("/controls")
 
if __name__ == '__main__': 
    app.run(debug=True, host='0.0.0.0', port=port)
