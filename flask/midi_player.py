## ANATOMY FOR PLAYER
## Play, pause, stop, back, forward
## get_current_note
## load, loop

import pygame.midi
import os.path
import time
import mido
import midi
from threading import Timer

## ANATOMY OF A TRACK:
### track has a header w metadata, rate info etc
### track has body which is the sony itself
#### body is made up of series of noteon/noteoff commands
#### each of those has a note + a tick associated with
class midi_player:
	def __init__(self):
		pygame.midi.init()
		self.muted = False
		self.playing = False
		self.lastTime = time.time()
		self.stepTime = .5
		self.trackIndex = 0
		self.activeNotes = ['  ' for x in range(128)]
		self.trck = -1
		self.dtime = 0
		self.player = pygame.midi.Output(0)
		self.player.set_instrument(1,1)
		self.tempo = 1
		self.ticks = 1
		self.ser = None
		self.trackTime = 0

	def load(self,url):
		ext = url.split('.')
		if os.path.isfile(url) and (ext[-1]  == 'mid' or  ext[-1]  == 'midi'):
			print("loaded file named {0}".format(url))
			self.track = midi.read_midifile(url)
			print(self.track[0])
			header = self.track[0]
			for f in header:
				if isinstance(f,midi.SetTempoEvent):
					self.tempo = f.get_mpqn()
					self.tempo /= self.track.resolution
					print(self.tempo)
					break
		else:
			print("WARNING this doesn't appear to be a valid midi file..") 
			self.track = -1
	def play(self):
		if self.track != -1 :
			self.playing = True
			self.doStep()
			return 1
		else:
			print("WARNING no track to play")
			return -1	
	def pause(self):
		self.playing = False
		self.silence()
	def stop(self):
		self.trackIndex = 0
		self.pause() #does same as pause but also resets track index
	def set_serial(self,ser):
		self.ser = ser
	def doStep(self):
		if(self.playing):
			self.trackIndex +=1
			song = self.track[1]
			if(self.trackIndex >= len(song)):
				print("song finished")
				self.stop()
			else:
				instruction = song[self.trackIndex]
				print(instruction)
				if isinstance(instruction, midi.NoteOnEvent):
					self.noteOn(instruction.data)
					pitch,velocity = instruction.data

					if self.ser.is_open:
						leds = [0 for x in range(66)]
						pitch, velocity = instruction.data
						pitch %= 22
						
						leds[pitch*3] =126
						leds[pitch*3+1] =126
						leds[pitch*3+2] =126
						print(pitch)
						self.ser.write(leds)
				elif isinstance(instruction, midi.NoteOffEvent):
					self.noteOff(instruction.data)
				#now catch any 0 wait time
				self.stepTime = instruction.tick*self.tempo/1000000
				t = Timer(self.stepTime, self.doStep)
				t.start()
	def stepBack(self):
		self.silence()
	def noteOn(self, note):
		pitch,velocity = note
		notes = [x for x in range(128)]
		self.activeNotes[pitch] = str(pitch)
		if not self.muted:
			self.player.note_on(pitch,velocity,1)
		self.displayNotes()
	def noteOff(self, note):
		pitch,velocity = note
		notes = [x for x in range(128)]
		self.activeNotes[pitch] = "  "
		self.player.note_off(pitch,127,1)
		# self.displayNotes()
	def displayNotes(self):		
		s = "==="
		for c in self.activeNotes[33:100]:
			s += c
		s += "==="
		# print(s)
	def mute(self):
		self.muted = True
		self.silence()
	def unmute(self):
		self.muted = False
	def toggleMute(self):
		if self.muted:
			self.unmute()
		else:
			self.mute()
	def silence(self):
		for x in range(128):
			self.player.note_off(x,127,1)
	def run(self):
		if self.playing:
			self.trackTime += self.tempo
		t = Timer(self.tempo,self.run)
		t.start()

def get_midi_player():
	return midi_player()
